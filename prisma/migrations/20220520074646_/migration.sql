/*
  Warnings:

  - You are about to drop the column `owner` on the `Module` table. All the data in the column will be lost.
  - Added the required column `changelog` to the `Release` table without a default value. This is not possible if the table is not empty.
  - Added the required column `supports` to the `Release` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Module" DROP COLUMN "owner";
ALTER TABLE "Module" ADD COLUMN     "hidden" BOOL NOT NULL DEFAULT false;
ALTER TABLE "Module" ADD COLUMN     "owners" STRING[];

-- AlterTable
ALTER TABLE "Release" ADD COLUMN     "changelog" STRING NOT NULL;
ALTER TABLE "Release" ADD COLUMN     "releaseDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE "Release" ADD COLUMN     "supports" INT8 NOT NULL;
