-- CreateTable
CREATE TABLE "Module" (
    "id" INT8 NOT NULL DEFAULT unique_rowid(),
    "name" STRING NOT NULL,
    "description" STRING NOT NULL,
    "owner" STRING NOT NULL,

    CONSTRAINT "Module_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Release" (
    "moduleId" INT8 NOT NULL,
    "release" STRING NOT NULL,

    CONSTRAINT "Release_pkey" PRIMARY KEY ("moduleId","release")
);

-- AddForeignKey
ALTER TABLE "Release" ADD CONSTRAINT "Release_moduleId_fkey" FOREIGN KEY ("moduleId") REFERENCES "Module"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
