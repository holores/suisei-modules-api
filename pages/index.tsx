import NavBar from '../components/Navbar';
import Footer from '../components/Footer';

export default function HomePage() {
	return (
		<div
			className="min-w-screen max-w-screen flex min-h-screen flex-col"
			data-theme="dark"
		>
			<NavBar />
			<div className="grow px-36 py-8">
				<p>Hello</p>
			</div>
			<Footer />
		</div>
	);
}
