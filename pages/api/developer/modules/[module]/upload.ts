import type { NextApiRequest, NextApiResponse } from 'next';
import { PrismaClient } from '@prisma/client';
import * as Minio from 'minio';

const prisma = new PrismaClient();
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const minioClient = new Minio.Client({
	endPoint: process.env.MINIO_URL!,
	port: process.env.MINIO_PORT ? Number.parseInt(process.env.MINIO_PORT, 10) : 9000,
	accessKey: process.env.MINIO_ACCESS_KEY!,
	secretKey: process.env.MINIO_SECRET_KEY!,
	useSSL: process.env.MINIO_SSL === 'true',
});

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const modules = await prisma.module.findMany();
	res.json(modules);
}
