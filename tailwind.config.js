module.exports = {
	darkMode: 'class',
	content: [
		'./pages/**/*.{js,ts,jsx,tsx}',
		'./components/**/*.{js,ts,jsx,tsx}',
	],
	theme: {},
	daisyui: {
		themes: [
			{
				light: {
					primary: '#3b66ed',
					'primary-content': '#F7F4EA',
					secondary: '#00A0F2',
					'secondary-content': '#F7F4EA',
					accent: '#091540',
					neutral: '#191D24',
					'base-100': '#F7F4EA',
					'base-content': '#000',
					info: '#00A0F2',
					success: '#9EF48A',
					warning: '#F9F871',
					error: '#FF626C'
				},
			},
			{
				dark: {
					primary: '#3b66ed',
					'primary-content': '#F7F4EA',
					secondary: '#3D518C',
					'secondary-content': '#fff',
					accent: '#091540',
					neutral: '#191D24',
					'base-100': '#2A303C',
					info: '#00A0F2',
					success: '#9EF48A',
					warning: '#F9F871',
					error: '#FF626C'
				},
			},
		],
	},
	plugins: [require('daisyui')],
};
