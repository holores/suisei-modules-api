import Link from 'next/link';

export default function NavBar() {
	return (
		<div className="navbar h-[4.5rem] px-8 py-4 bg-secondary text-secondary-content">
			<div className="navbar-start">
				<Link passHref href="/">
					<h1 className="cursor-pointer text-2xl">
						Suisei&apos;s Mic - Module catalog
					</h1>
				</Link>
			</div>
			<div className="navbar-end">
				<p className="cursor-not-allowed btn btn-primary">
					Developer Portal
				</p></div>
		</div>
	);
}
