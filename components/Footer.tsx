export default function Footer() {
	return (
		<div className="flex h-[4.5rem] w-full flex-col items-center justify-center px-8 py-4">
			<div className="flex items-center gap-2 text-center">
				<a href="https://docs.suisei.app" target="_blank" rel="noreferrer">Documentation</a>
				<span>|</span>
				<a href="https://bitbucket.org/holores/workspace/projects/SUI" target="_blank" rel="noreferrer">
					Source Code
				</a>
				<span>|</span>
				<p className="cursor-not-allowed">Developer Portal</p>
			</div>
		</div>
	);
}
